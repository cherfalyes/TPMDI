package fr.esir.mdi.ci.tpmaven;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testaddContent() throws DocumentException
    {
    	Document document = new Document();
    	Document Empty = new Document();
    	FirstPdf.addContent(document);
    	String documentContent = document.getHtmlStyleClass();
    	assertTrue(documentContent.length() > 0);
    }
    
    @Test 
    public void testAddMetaData() throws DocumentException
    {
    	Document document = new Document();
    	Document Empty = new Document();
    	FirstPdf.addMetaData(document);
    	String documentContent = document.getHtmlStyleClass();
    	System.out.println(documentContent);
    	System.out.println();
    	assertTrue(documentContent.length() > 0);
    }
    
    @Test
    public void testAddTitlePage() throws DocumentException
    {
    	Document document = new Document();
    	Document Empty = new Document();
    	FirstPdf.addTitlePage(document);
    	String documentContent = document.getHtmlStyleClass();
    	assertTrue(documentContent.length() > 0);
    }
    
    public static void main(String args[])
    {
    	
    }
    
}

